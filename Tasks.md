# Tasks

The class `MeetingSession` contains all details about the meeting session.

> You do not have to manually create these sessions : they are already implemented in the front-end for development purposes. You will have a specific credentials for the backend API and will get dummy attendees as example.

Ticked tasks have to be done in priority. 

This is a regular React project, `npm i` and `npm start` will do the job.

## Links

- [AWS Chime NodeJS SDK](https://github.com/aws/amazon-chime-sdk-js)

## Attendee actions

- [x] Configure devices (audio input, video input and audio output).
- [x] Detects when a new device is added : automatically switch to the device if there are no others devices (for example, no audio output devices found and the user suddenly plug-in his earpods, meeting must automatically select this new device as default).
- [x] Enable mute and unmute.
- [x] Enable video recording.
- [ ] Screen sharing (moderators).
- [ ] Mute others attendees (moderators).
- [ ] Force mute others attendees (moderators).
- [ ] Force webcam disable others attendees (moderators).

## Videos and screen sharing content

- [x] If there are no videos, blank screen (as it is by default).
- [x] If there is at least one video, each video must be rendered as responsive blocks (automaticall stacked by row and columns). At maximum, 9 videos can be shown at the same time. If there are more than 9 videos at the same time, we simply do not show the others above 9.
- [ ] If there is at least one screen sharing, a first row (~20% of viewport height) is for videos stacked horizontally, and the second row (~80% of viewport height) is for screen content. Attendees should be allowed to resize proportion of first and second rows.
- [ ] We do not allow two screens sharing at the same time. If a moderator tries to screen share although an other moderator screen shares, a message will prevent him to do it.

## Chat

- Work in Progress