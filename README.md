# Guide

This guide shows you basics concepts of the virtual classroom.

## Global View

This app is intended to reproduce a virtual classroom.

![Interface of virtual classroom](resources/app.png)

This interface is splitted into three blocks.

- **Content** : central block will display videos, screen sharing and a toolbar to mute/unmute, enable/disable record video, screen share button and parameters.
- **Attendees** : List of attendees, including moderators, registered guests and unregistered guests.
- **Chat** : Any attendee (moderator, registered or unregistered guest) can post messages.

## Attendees roles

Each attendee has a specific role that is retrieved from the backend. Those roles define how they can interact during the meeting.

### Moderators

Moderators are often the *hosts* of the meeting, so they commonly have a full control. They can :

- Share their screen
- Interrupt video input recording of any attendee (including other moderator)
- Interrupt audio input recording of any attendee (including other moderator)
- Mute everyone
- Force mute a user (force mute means that the user will not be able to unmute himself) (a moderator cannot be force muted)

### Registered guests

Registered guests are guests that was invited by the moderators and have a special access to the meeting. They can :

- Speak and record video
- Mute and unmute their own audio input
- Start and stop video recording

Registered attendees cannot :

- Share screen
- Mute/Unmute others attendees

### Unregistered Guests

Unregistered guests are guests that was invited by the moderators but have no special access to the meeting. They can :

- Send message to chat
- View videos and screen shared content

Unregistered guests cannot :

- Speak or record video
- Share their own screen